// - 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

// const data = require("./data-1");
function countAllPeople(data) {
  try { 
    if(!data || !data.houses){  
      return new Error("Invalid data format: houses array not found.");
    }
    let houses = data.houses;
    let total = houses.reduce((accumulator, current, index, array) => {
      if( !current || !current.people  ){
        return new Error("Invalid data format: people array not found")
      }
      let people = current.people;
      let count = 0;
      people.forEach((element) => {
        if( !element  ||  !element.name ){
          return new Error("Invalid data format: current.people not fount")
        }
        if (element.name != undefined || element.name == "") {
          count += 1;
        }
      });
      return accumulator + count;
    }, 0);

    // console.log(total);
    return total;
  } catch (error) {
    console.error("inside error :- ", error);
  }
}

// countAllPeople(data); 


// - 2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

// const data = require("./data-1");
function peopleByHouses(data) {
  let houses = data.houses;
  let count = houses.reduce((accumulator, current) => {
    let people = current.people;
    let totalCountInFamily = people.reduce((prev, curr) => {
      if (curr.name != undefined || curr.name == "") {
        return prev + 1;
      }
    }, 0); 
    accumulator[current.name] = totalCountInFamily ;
    return accumulator;
  }, {}); 
  return count
//   console.log(count);
}

// peopleByHouses(data); 




// - 3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.
function everyone(data) {
  try {
    if (!data || !data.houses) {
      return new Error("Invalid data format: houses array not found.");
    }
    let houses = data.houses;
    let totalNames = houses.reduce((accumulator, current, index, array) => {
      if (!current || !current.people) {
        return new Error("Invalid data format: people array not found");
      }
      let people = current.people;
      people.forEach((element) => {
        if (element.name != undefined || element.name == "") {
          accumulator.push(element.name);  // pushing all the names to a []
        }
      });
      return accumulator; // return a final added. with the previous  value
    }, []);

    // console.log(totalNames);
    return totalNames
  } catch (error) {
    console.log("error occured ", error);
  }
}


// - 4. Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

// const data = require("./data-1");
function nameWithS(data) {
  try {
    if (!data || !data.houses) {
      return new Error("Invalid data format: houses array not found.");
    }
    let houses = data.houses;
    let totalNames = houses.reduce((accumulator, current, index, array) => {
      let head = current.name.toLowerCase();
      if (!current || !current.people) {
        return new Error("Invalid data format: people array not found");
      }
      let people = current.people;
      people.forEach((element) => {  // iterating through all the people array. 
        let name = element.name.toLowerCase(); 
        if (name.includes("s")) {  
          accumulator.push(element.name);
        }
      });
      return accumulator;
    }, []);

    // console.log(totalNames);
    return totalNames
  } catch (error) {
    console.log("error occured :-", error);
  }
}
// nameW ithS(data);




// - 5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

// const data = require("./data-1");
function nameWithA(data) {
  if (!data || !data.houses) {
    return new Error("Invalid data format: houses array not found.");
  }
  let houses = data.houses;
  let totalNames = houses.reduce((accumulator, current, index, array) => {
    if( !current || !current.people  ){
      return new Error("Invalid data format: people array not found")
    }
    let head = current.name.toLowerCase();
    let people = current.people;
    people.forEach((element) => {
      let name = element.name.toLowerCase();  // need to find out whether the name includes A or a . so it should return both. so made it to toLowerCase()
      if (name.includes("a")) { 
        accumulator.push(element.name);
      }
    });
    return accumulator;
  }, []);

  // console.log(totalNames);
  return totalNames
}

// console.log(nameWithA(data));




// - 6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

// const data = require("./data-1");
function surnameWithS(data) {
  if (!data || !data.houses) {
    return new Error("Invalid data format: houses array not found.");
  }
  let houses = data.houses;
  let totalNames = houses.reduce((accumulator, current, index, array) => {
    if( !current || !current.people  ){
      return new Error("Invalid data format: people array not found")
    }
    let people = current.people;
    people.forEach((element) => {
      if(!element  || !element.name){
        return new Error("Invalid data format: element array not found")
      }
      let name = element.name;
      name = name.split(" "); // surname actually ends in the last with space. so splitting and finding out the last word.
      let surname = name[name.length - 1]; 
      if (surname.startsWith("S")) {
        accumulator.push(element.name);
      }
    });
    return accumulator;
  }, []);

  // console.log(totalNames);
  return totalNames
}

// surnameWithS(data);



// - 7. Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).
// const data = require("./data-1");
function surnameWithA(data) {
  if (!data || !data.houses) {
    return new Error("Invalid data format: houses array not found.");
  }
  let houses = data.houses;
  let totalNames = houses.reduce((accumulator, current, index, array) => {
    if( !current || !current.people  ){
      return new Error("Invalid data format: people array not found")
    }
    let people = current.people;
    people.forEach((element) => {
      if(!element  || !element.name){
        return new Error("Invalid data format: element array not found")
      }
      let name = element.name;
      name = name.split(" "); // surname actually ends in the last with space. so splitting and finding out the last word.
      let surname = name[name.length - 1]; 
      if (surname.startsWith("A")) { // if names start with a push it to an Arrray.
        accumulator.push(element.name);
      }

    });
    return accumulator;
  }, []);

  // console.log(totalNames);
  return totalNames
}

// surnameWithA(data);




// - 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.
// const data = require("./data-1");
function peopleNameOfAllHouses(data) {
  if (!data || !data.houses) {
    return new Error("Invalid data format: houses array not found.");
  }
  let houses = data.houses;
  let finalNames = houses.reduce((accumulator, current, element, array) => {
    if( !current || !current.people  || current.head ){
      return new Error("Invalid data format: people or head  array not found")
    }
    let head = current.name;
    let people = current.people;
    let homies = people.reduce((prev, curr) => {
      prev.push(curr.name); // pushing all the names to an array.
      return prev;
    }, []);
    accumulator[head] = homies; // now assigning the exact house name with people names.
    return accumulator;
  }, {});
  //   console.log(finalNames)
  return finalNames;
}
// console.log(peopleNameOfAllHouses(data));
module.exports = {peopleByHouses , countAllPeople ,everyone , nameWithS  , nameWithA ,surnameWithS ,surnameWithA , peopleNameOfAllHouses };